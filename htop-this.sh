#!/bin/bash
executablefullpath=$1
executablefilename=$(basename -- "$executablefullpath")
eval "$executablefullpath &"
executableprocessid=$(ps --ppid $$ | awk "/$executablefilename/ { print \$1 }")
htop `echo "-p$executableprocessid"`
